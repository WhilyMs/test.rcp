package test.rcp.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;


public class SwitchPerspectiveTracingHandler {
	
	@Inject EPartService partService;
	@Inject EModelService modelService;

	@Execute
	public void execute(MApplication app) {
		MPerspective element = (MPerspective) modelService.find("test.rcp.perspective.tracing", app);
		partService.switchPerspective(element);
	}
	
}